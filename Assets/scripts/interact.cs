﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class interact : MonoBehaviour {
    public string interactButton;
    public float interactDistance = 3f;
    public LayerMask interactLayer;//filter
    public Image interactIcon;
    public Image interactIcon2;
    public bool isInteracting;
    


    // Use this for initialization
    void Start () {
        if (interactIcon != null)
        {
            interactIcon.enabled = false;
            interactIcon2.enabled = false;
        }
	}
	
	// Update is called once per frame
	void Update () {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;
        //shoots a ray
        if(Physics.Raycast(ray,out hit, interactDistance, interactLayer))
        {
            if (isInteracting == false)
            {
                if (interactIcon != null)
                {
                    interactIcon.enabled = true;
                    interactIcon2.enabled = true;
                }

                if (Input.GetButtonDown(interactButton) || CrossPlatformInputManager.GetButtonDown(interactButton) )//|| CrossPlatformInputManager.GetButtonDown("Fire2"))
                {
                    if (hit.collider.CompareTag("Door"))
                    {
                        hit.collider.GetComponent<Door>().ChangeDoorState();
                    }
                    else if(hit.collider.CompareTag("Key"))
                    {
                        hit.collider.GetComponent<key>().UnlockDoor();
                        
                    }
                    else if (hit.collider.CompareTag("Safe"))
                    {
                       
                        //hit.collider.GetComponent<Safe>().ShowSafeCanvas();
                    }
                    else if (hit.collider.CompareTag("Note"))
                    {
                        hit.collider.GetComponent<Note>().ShowNoteImage();
                    }
                    else if (hit.collider.CompareTag("Pistol"))
                    {
                        hit.collider.GetComponent<PistolPickup>().PickupPistol();
                    }

                }
            }
        }
        else
        {
            interactIcon.enabled = false;
            interactIcon2.enabled = false;
        }
	}
}
