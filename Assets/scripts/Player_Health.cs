﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Player_Health : MonoBehaviour {
    public int MaxHealth=100;
    private int currentHealth=0;
	// Use this for initialization
	void Start () {
        currentHealth = MaxHealth;
	}
	
	public void takeDamage(int _damage)
    { currentHealth -= _damage;
        if (currentHealth <= 0)
        {
            Die();
        }
    }
	void Die()
    {

        SceneManager.LoadScene("scene");
    }
}
