﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Enemy_Health : MonoBehaviour {
    public int maxHealth = 100;
    private int currentHealth;
    private Animator animator;
    private Enemy_Chase enemy_chase;
    private AudioSource audioSource;
    public AudioClip growlSound;
	// Use this for initialization
	void Start () {
        audioSource = GetComponent<AudioSource>();
        currentHealth = maxHealth;
        animator = GetComponent<Animator>();
        animator.SetBool("isDead", false);
        enemy_chase = GameObject.FindGameObjectWithTag("Enemy").GetComponent<Enemy_Chase>();
    }
	
	public void TakeDamage(int _damage)
    {
        currentHealth -= _damage;
        animator.SetTrigger("isHit");
        
        if (currentHealth <= 0)
        {
            Die();
          //  audioSource.PlayOneShot(growlSound);

            Invoke("loadscene", 12);
            // SceneManager.LoadScene("MainMenu");
        }
    }
    void Die()
    {
        animator.SetBool("isDead", true);
        animator.SetBool("isChasing", false);
        Destroy(enemy_chase);
        
        // Destroy(gameObject);
    }
    void loadscene()
    {
         SceneManager.LoadScene("MainMenu");
    }
}
