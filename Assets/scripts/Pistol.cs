﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol : MonoBehaviour {
    public int damage = 25;
    public int ammo = 0;
    public float range = 50;

    private Transform mainCamera;

    private AudioSource myAudioSource;
    public AudioClip shootSound;
    public AudioClip EmptyClip;
    
    // Use this for initialization
	void Start () {
        
        myAudioSource = GetComponent<AudioSource>();
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").transform;   
	}
	
	// Update is called once per frame
	void Update () {
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").transform;
        if (Input.GetButtonDown("Fire1")&& ammo > 0)
        {
             Shoot();
           
        }else if (Input.GetButtonDown("Fire1") && ammo >= 0)
        {
            myAudioSource.PlayOneShot(EmptyClip);
        }
	}
    void Shoot()
    {
      
        Ray ray = new Ray(mainCamera.position, mainCamera.forward);
        RaycastHit hit;
   

       
           if (Physics.Raycast(ray,out hit, range))
        {
            if (hit.collider.CompareTag ("Enemy")){
                //damage the enemy
                hit.collider.GetComponent<Enemy_Health>().TakeDamage(damage);
            }
        }
        myAudioSource.PlayOneShot(shootSound);
        ammo--;
    }
    public void addAmmo()
    {
        ammo = 12;
    }
}
