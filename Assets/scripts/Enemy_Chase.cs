﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class Enemy_Chase : MonoBehaviour {

    private  NavMeshAgent myAgent;
    private Animator myAnimator;
    public Transform target;

    public bool chaseTarget = true;
    public float stopingDistance = 2.5f;
    public float delayBetweenAttacks = 1.5f;
    private float attackCooldown;
    private float distanceFromTarget;
    public int damage=20;
    private Player_Health playerHealth;
    private AudioSource audioSource;
    public AudioClip leftAttackSound;
    public AudioClip rightAttackSound;
    public AudioClip growlsound;
	// Use this for initialization
	void Start () {
        audioSource = GetComponent<AudioSource>();
        myAgent = GetComponent<NavMeshAgent>();
        myAnimator = GetComponent<Animator>();
        myAgent.stoppingDistance = stopingDistance;
        attackCooldown = Time.time;
        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Health>();

	}
	
	// Update is called once per frame
	void Update () {
        ChaseTarget();
	}
    
    void ChaseTarget(){
        distanceFromTarget = Vector3.Distance(target.position, transform.position);
        if(distanceFromTarget >= stopingDistance)
        {
            chaseTarget = true;
        }
        else
        {
            chaseTarget = false;
            Attack();

        }

        if (chaseTarget)
        {
            myAgent.SetDestination(target.position);
            myAnimator.SetBool("isChasing",true);
        }
        else
        {
            myAnimator.SetBool("isChasing",false);
        }
    }
    void Attack()
    {
        if(Time.time>attackCooldown)
        {
            playerHealth.takeDamage(damage);
            myAnimator.SetTrigger("Attack");
            attackCooldown = Time.time + delayBetweenAttacks;
            audioSource.PlayOneShot(leftAttackSound);
            audioSource.PlayOneShot(rightAttackSound);
            audioSource.PlayOneShot(growlsound);
        }
    }
}
