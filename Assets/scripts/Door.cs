﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class Door : MonoBehaviour {
    public bool open = false;
    public bool openedCompletly = false;
    public AudioSource audiosource;
    public AudioClip doorClip;
    public AudioClip lockedDoorClip;
    public float doorOpenAngle = 90f;
    public float doorCloseAngle = 0f;
    public float speed = 2f;//speed of rotation
    public bool test=false;
    public bool isLocked=false;
    void start()
    {
        audiosource = GetComponent<AudioSource>();
    }
    public void ChangeDoorState()
    {
        if (isLocked == false)
        {
            open = !open;

            if (audiosource != null)
            {
                audiosource.PlayOneShot(doorClip);
            }
        }
        else
        {
            if (audiosource != null)
            {
                audiosource.PlayOneShot(lockedDoorClip);
            }
        }
    }

    // Update is called once per frame
    void Update() {

        if(RigidbodyFirstPersonController.doorName.Equals(transform.gameObject.name))
        {
            open = !open;

            if (audiosource != null)
            {
                audiosource.PlayOneShot(doorClip);
            }

            RigidbodyFirstPersonController.doorName = "";
        }

        if (open)
        {
            if (openedCompletly == false)
            {
                Quaternion targetRotationOpen = Quaternion.Euler(0, doorOpenAngle, 0);
                transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRotationOpen, speed * Time.deltaTime);

                if (transform.localRotation == targetRotationOpen)
                {
                    openedCompletly = true;
                }
            }
        }
        else
        {
            Quaternion targetRotationClose = Quaternion.Euler(0, doorCloseAngle, 0);
            transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRotationClose, speed * Time.deltaTime);
            openedCompletly = false;
        }
        if (test == true)
        {
            ChangeDoorState();
            test = false;
        }
	}
}
