﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolPickup : MonoBehaviour {

    public AudioClip pickupSound;
    public GameObject pistol;

    public GameObject zombie;
    public GameObject zombieModel;
   // public GameObject Cursor;
    
	public void PickupPistol()
    {
        GetComponent<AudioSource>().PlayOneShot(pickupSound);
        //  pistol.SetActive(true);
        pistol.GetComponent<Pistol>().addAmmo();
        zombie.SetActive(true);
        zombieModel.SetActive(false);
        //Cursor.SetActive(true);

        Destroy(gameObject, pickupSound.length);
    }
}
