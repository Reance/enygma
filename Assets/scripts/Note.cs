﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
public class Note : MonoBehaviour {

    public Image noteImage;
    public Image noteImage2;
    public AudioClip pickupSound;
    public AudioClip putAwaySound;
    
    // Use this for initialization
    void Start () {
        noteImage.enabled = false;
        noteImage2.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
        if(noteImage.enabled)
        if (Input.GetButtonDown("Cancel"))
        {
            HideNoteImage();
        }
    }
    public void ShowNoteImage()
    {
        noteImage.enabled = true;
        noteImage2.enabled = true;
        GetComponent<AudioSource>().PlayOneShot(pickupSound);
        

    }
    public void HideNoteImage()
    {
        noteImage.enabled = false;
        noteImage2.enabled = false;
        GetComponent<AudioSource>().PlayOneShot(putAwaySound);
        
    }
}
