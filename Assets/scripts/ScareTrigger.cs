﻿using UnityEngine;
using System.Collections;

public class ScareTrigger : MonoBehaviour {
    public AudioSource audioSource;
    public AudioClip pianoSound;
    public Light spotLight;
    private bool hasSoundPlayed;
    void Start()
    {
        spotLight.enabled = false;
    }
    void OnTriggerEnter(Collider other)
    {
        
        if (other.CompareTag("Player") && hasSoundPlayed == false)
        {
            audioSource.PlayOneShot(pianoSound);
            spotLight.enabled = true;
            hasSoundPlayed = true;
        }
    }

}
