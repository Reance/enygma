﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class key : MonoBehaviour {
    public Door myDoor;
    private AudioSource audiosource;
    public AudioClip keyClip;
    void Start()
    {
        audiosource = GetComponent<AudioSource>();
    }
    public void UnlockDoor()
    {
        
        myDoor.isLocked = false;
        audiosource.PlayOneShot(keyClip);

        StartCoroutine("WaitForSelfDestruct");
       }
        
    IEnumerator WaitForSelfDestruct()
    {
        yield return new WaitForSeconds(0.3f);
        Destroy(gameObject);
    }	
}
