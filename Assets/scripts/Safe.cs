﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;
	

public class Safe : MonoBehaviour {
    public Canvas safeCanvas;
    //public GameObject playerObject;

    private int number01 = 0;
    private int number02 = 0;
    private int number03 = 0;
    private int number04 = 0;
    private int number05 = 0;
    private int number06 = 0;
    private int number07 = 0;
    private int number08 = 0;

    public Text textNumber01;
    public Text textNumber02;
    public Text textNumber03;
    public Text textNumber04;
    public Text textNumber05;
    public Text textNumber06;
    public Text textNumber07;
    public Text textNumber08;
    public bool opened;
    public float doorOpenAngle = 90f;
    public float speed = 2f;//speed of rotation

    void Start()
    {
        //safeCanvas.enabled = false;
        textNumber01.text = number01.ToString();
        textNumber02.text = number02.ToString();
        textNumber03.text = number03.ToString();
        textNumber04.text = number04.ToString();
        textNumber05.text = number05.ToString();
        textNumber06.text = number06.ToString();
        textNumber07.text = number07.ToString();
        textNumber08.text = number08.ToString();
        opened = false;
    }
    /*public void ShowSafeCanvas()
    {
        safeCanvas.enabled = true;
        //disable the player controller
        playerObject.GetComponent<RigidbodyFirstPersonController>().enabled=false;

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }*/
    void Update () {
      /*  if (Input.GetButtonDown("Cancel"))
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            //enable player movement
            playerObject.GetComponent<RigidbodyFirstPersonController>().enabled = true;

            safeCanvas.enabled = false;
        }//checks the numbers match with pass*/
        if(number01==1 && number02==1 &&number03==1 && number04 == 2 || number05==4 && number06==5 && number07==4 && number08==5)
        {
            opened = true;
        }
        if (opened == true)
        {
            //Cursor.lockState = CursorLockMode.Locked;
            //Cursor.visible = false;
            //enable player movement
           // playerObject.GetComponent<RigidbodyFirstPersonController>().enabled = true;

           // safeCanvas.enabled = false;
            //gameObject.layer = 0;



            UnlockSafe();
        }
   
	}
    void UnlockSafe()
    {
        Quaternion targetRotationOpen = Quaternion.Euler(0, 0, doorOpenAngle);
        transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRotationOpen, speed * Time.deltaTime);
    }
    public void IncreaseNumber(int _number) {
        if (_number == 1)
        {
            number01++;
            textNumber01.text = number01.ToString();

            if (number01 > 9)
            {
                number01 = 0;
                textNumber01.text = number01.ToString();
            }
        }else if (_number == 2)
        {

            number02++;
            textNumber02.text = number02.ToString();

            if (number02 > 9)
            {
                number02 = 0;
                textNumber02.text = number02.ToString();
            }
        }else if (_number == 3)
        {

            number03++;
            textNumber03.text = number03.ToString();

            if (number03 > 9)
            {
                number03 = 0;
                textNumber03.text = number03.ToString();
            }
        }else if (_number == 4)
        {

            number04++;
            textNumber04.text = number04.ToString();

            if (number04 > 9)
            {
                number04 = 0;
                textNumber04.text = number04.ToString();
            }
        }else if (_number == 5)
        {
            number05++;
            textNumber05.text = number05.ToString();

            if (number05 > 9)
            {
                number05 = 0;
                textNumber05.text = number05.ToString();
            }
        }
        else if (_number == 6)
        {
            number06++;
            textNumber06.text = number06.ToString();

            if (number06 > 9)
            {
                number06 = 0;
                textNumber06.text = number06.ToString();
            }
        }
        else if (_number == 7)
        {
            number07++;
            textNumber07.text = number07.ToString();

            if (number07 > 9)
            {
                number07 = 0;
                textNumber07.text = number07.ToString();
            }
        }
        else if (_number == 8)
        {
            number08++;
            textNumber08.text = number08.ToString();

            if (number08 > 9)
            {
                number08 = 0;
                textNumber08.text = number08.ToString();
            }
        }

    }
    public void DecreaseNumber(int _number)
    {
        if (_number == 1)
        {
            number01--;
            textNumber01.text = number01.ToString();

            if (number01 < 1)
            {
                number01 = 9;
                textNumber01.text = number01.ToString();
            }
        }
        else if (_number == 2)
        {

            number02--;
            textNumber02.text = number02.ToString();

            if (number02 <1)
            {
                number02 = 9;
                textNumber02.text = number02.ToString();
            }
        }
        else if (_number == 3)
        {

            number03--;
            textNumber03.text = number03.ToString();

            if (number03 <1)
            {
                number03 = 9;
                textNumber03.text = number03.ToString();
            }
        }
        else if (_number == 4)
        {

            number04--;
            textNumber04.text = number04.ToString();

            if (number04 <1)
            {
                number04 = 9;
                textNumber04.text = number04.ToString();
            }
        }

    }
}
 