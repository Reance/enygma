﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.CrossPlatformInput;
public class MainMenu : MonoBehaviour {
    public float gazeTime = 200f;

    private float timer;
    private bool gazedAt; 
    void Start()
    {
        // Cursor.lockState = CursorLockMode.None;
        //Cursor.visible = true;
        gazedAt = true;
    }
    void Update()
    {
        
    }
public void LoadLevel(string _levelName)
    {
        while (gazedAt)
        {
            timer += Time.deltaTime;
            if (timer >= gazeTime || CrossPlatformInputManager.GetButtonDown("Fire1")|| Input.GetButtonDown("Fire1"))
            {
                SceneManager.LoadScene(_levelName);
                gazedAt = false;
            }

        }

       
    }

    public void Quit()
    {
       
        
            Application.Quit();
    }
}
