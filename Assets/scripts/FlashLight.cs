﻿using UnityEngine;
using System.Collections;

public class FlashLight : MonoBehaviour
{
    public Light flashLight;
    public AudioSource audioSource;

    private bool isActive;
    public AudioClip soundFlashOn;
    public AudioClip soundFlashOff;
    // Use this for initialization
    void Start()
    {
        isActive = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F)||Input.GetButtonDown("Fire2"))
        {
            if (isActive == false)
            {
                flashLight.enabled = true;
                isActive = true;
                audioSource.PlayOneShot(soundFlashOn);
            }
            else
            {
                flashLight.enabled = false;
                isActive = false;
                audioSource.PlayOneShot(soundFlashOff);
            }
        }
    }
}
