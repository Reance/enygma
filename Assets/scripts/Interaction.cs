﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class Interaction : MonoBehaviour {
    public float gazeTime = 2f;
   // public LayerMask interactLayer;
    private float timer;
  //  public float interactDistance = 3f;
    private bool gazedAt;
	// Use this for initialization
	void Start () {
     

	}
	
	// Update is called once per frame
	void Update () {
      //  Ray ray = new Ray(transform.position, transform.forward);
      //  RaycastHit hit;
       // if (Physics.Raycast(ray, out hit, interactDistance,interactLayer))
        {
            if (gazedAt)
           {
                timer += Time.deltaTime;
                if (timer >= gazeTime)
                {
                    ExecuteEvents.Execute(gameObject, new PointerEventData(EventSystem.current), ExecuteEvents.pointerDownHandler);
                    // ExecuteEvents.Execute(gameObject, new PointerEventData(EventSystem.current), ExecuteEvents.pointerClickHandler);
                    gazedAt = false;
                    timer = 0f;
                    // GetComponent<Collider>().enabled=false;
                }
            }
        }
	}
  public  void PointerEnter()
    {
        gazedAt = true;
    }
    public void PointerExit()
    {
        gazedAt = false;
    }
    public void PointerDown()
    {
        gazedAt = false;
    }
}
